<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title></title>
  </head>
  <body>

      <?php

       $weather = array("Sunshine", "Clouds", "Rain", "Hail", "Sleet", "Snow", "Wind", "Heat");
       $season = array("Spring", "Summer", "Autumn", "Winter");

       function selectw($array)
       {
         for( $i=0 ; $i<count($array) ; $i++){
           echo "<input type='checkbox' name='weather[]' value='$array[$i]'>".$array[$i]."<br>";
         };
       };
       function selects($array)
       {
         for( $i=0 ; $i<count($array) ; $i++){
           echo "<input type='checkbox' name='season[]' value='$array[$i]'>".$array[$i]."<br>";
         };
       };

       ?>

       <h1>Your Favorite Seasons and Weather are:-</h1>
       <p>Please enter the city of your choice :</p>

      <form class="" action="./action.php" method="post">

        <p>City: <input type="text" name="city" pattern="[a-zA-Z]+"></p>
        <p>Please choose kind of seasons and weather you like from the list below.</p>
        <p>Choose all that apply.</p>

        <h3>Weather</h3>

        <?php
         selectw($weather);
         ?>

        <h3>Season</h3>

        <?php
         selects($season);
         ?>

      <br>
      <input type="submit" name="submit" value="Submit">

     </form>

  </body>
</html>
