<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title></title>
  </head>
  <body>
    <h1 align="center">Register</h1>
    <form class="" action="register_success.php" method="post">
      <table align="center">
        <tr>
          <td>ID</td>
          <td><input type="text" name="loginid" required></td>
        </tr>
        <tr>
          <td>Password</td>
          <td><input type="password" name="pass" value="" required></td>
        </tr>
        <tr>
          <td>Address</td>
          <td><input type="text" name="address" value="" required></td>
        </tr>
        <tr>
          <td>Phone</td>
          <td><input type="tel" name="phone" value="" required></td>
        </tr>
        <tr>
          <td>Email</td>
          <td><input type="email" name="email" value="" required></td>
        </tr>
        <tr>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td>&nbsp;</td>
          <td><input type="submit" name="submit" value="Submit"></td>
        </tr>
      </table>
    </form>
  </body>
</html>
